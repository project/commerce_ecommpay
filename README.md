## Commerce ECOMMPAY for Drupal
The commerce_ecommpay module integrates ECOMMPAY payment solutions with Drupal Commerce, providing a seamless checkout experience for Drupal-based e-commerce sites. This module allows you to accept payments from various payment methods, ensuring secure and efficient transactions for your customers.

This module currently implements Payment Page integration (as per https://developers.ecommpay.com/en/en_PP_about.html), whereby the user is redirected to the payment form hosted by ECOMMPAY.

## Installation
To install the commerce_ecommpay module, run the following command in your Drupal project directory:

```
composer require drupal/commerce_ecommpay
```

This command will download and install the module and its dependencies. After installing, enable the module through the Drupal admin interface or by using Drush:

```
drush en commerce_ecommpay -y
```

## Configuration

After installation, configure the module by navigating to the Commerce ECOMMPAY settings form in your Drupal administration area. Here, you can enter your ECOMMPAY integration details and customize the payment methods available to your customers.

## Accessing the Configuration Form
1. Go to the Commerce > Configuration > Payment > Payment gateways configuration section in your Drupal admin area.
2. Select the Add payment gateway button.
3. Choose ECOMMPAY from the list of available payment gateways.

### Configuration Fields
- Project ID: Your unique identifier provided by ECOMMPAY.
- Secret Key: A secure key provided by ECOMMPAY.
- Enabled Payment Methods: Select the payment methods you wish to offer to your customers. This selection should match the payment methods enabled for your ECOMMPAY account.

Ensure that you save your configuration after entering your details.

## Two step payments - authorization hold
The module supports authorization hold separated from manual capture or void steps. This feature can be configured by changing the Payment Process pane settings for your selected Commerce Checkout flow.

This setting can be accessed under Commerce > Configuration > Orders > Checkout flows.
