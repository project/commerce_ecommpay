<?php

namespace Drupal\commerce_ecommpay\Plugin\Commerce\PaymentGateway;

use Drupal;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use ecommpay\Callback;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_ecommpay\EcommpayClient;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ecommpay",
 *   label = "Ecommpay",
 *   display_label = "Checkout Page",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_ecommpay\PluginForm\EcommpayForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Ecommpay extends OffsitePaymentGatewayBase implements SupportsRefundsInterface, SupportsAuthorizationsInterface {

  private const DRUPAL_STATUS_AUTHORIZATION = 'authorization';
  private const DRUPAL_STATUS_AUTHORIZATION_VOIDED = 'authorization_voided';

  private const DRUPAL_STATUS_COMPLETED = 'completed';

  private const SECRET_KEY_MASK = '************';

  private const STATUS_PARTIALLY_REFUNDED = 'partially_refunded';

  private const STATUS_SUCCESS = 'success';

  /**
   * The ECOMMPAY client.
   *
   * @var Drupal\commerce_ecommpay\EcommpayClient
   */
  protected $client;

  /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param $pluginId
   * @param $pluginDefinition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\commerce_payment\PaymentTypeManager $paymentTypeManager
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $paymentMethodTypeManager
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface|null $minorUnitsConverter
   * @param \Drupal\commerce_ecommpay\EcommpayClient|null $client
   *   The ECOMMPAY client.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    PaymentTypeManager $paymentTypeManager,
    PaymentMethodTypeManager $paymentMethodTypeManager,
    TimeInterface $time,
    MinorUnitsConverterInterface $minorUnitsConverter = null,
    EcommpayClient $client = null) {
    parent::__construct(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $entityTypeManager,
      $paymentTypeManager,
      $paymentMethodTypeManager,
      $time,
      $minorUnitsConverter);

    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('ecommpay.ecommpay_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'project_id' => '',
        'secret_key' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['project_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Project ID'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['project_id'],
    ];

    $keyString = (empty($this->configuration['secret_key'])?
      'The current secret key is not set.':
      'Don\'t change this field to keep the current secret key.');


    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#description' => $this->t($keyString),
      '#required' => FALSE,
      '#default_value' => !empty($this->configuration['secret_key']) ? self::SECRET_KEY_MASK : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    parent::submitConfigurationForm($form, $form_state);

    if ($form_state->getErrors()) {
      return;
    }

    $this->configuration['project_id'] = $values['project_id'];

    if (!empty($values['secret_key'])
      && $values['secret_key'] !== self::SECRET_KEY_MASK) {
      $this->configuration['secret_key'] = $values['secret_key'];
    }
    else {
      $oldConfig = $this->getConfiguration();
      $this->configuration['secret_key'] = $oldConfig['secret_key'];
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $config = $this->getConfiguration();

    // Get payment_id from request.
    $paymentId = $request->query->get('payment_id');
    if (empty($paymentId)) {
      Drupal::logger('commerce_ecommpay')->error('Missing payment_id');
      throw new InvalidResponseException('Missing payment_id');
    }

    $payment = $this->getPaymentById($paymentId);

    if (in_array($payment->getState(), [
      self::DRUPAL_STATUS_AUTHORIZATION,
      self::DRUPAL_STATUS_COMPLETED,
      true
    ])) {
      return;
    }

    // If local payment state is not completed,
    // please check the state using Gate API.
    $state = $this->client->getPaymentState((int) $config['project_id'], $config['secret_key'], $paymentId);

    if (!in_array($state, [Callback::AW_CAP_STATUS, Callback::SUCCESS_STATUS])) {
      Drupal::logger('commerce_ecommpay')->info('Payment was not successful.');
      throw new DeclineException('The was a problem processing you payment. Please try again.');
    }

    // Set correct state on Drupal payment entity.
    switch ($state) {
      case Callback::AW_CAP_STATUS:
        $payment->setState(self::DRUPAL_STATUS_AUTHORIZATION);
        break;
      case Callback::SUCCESS_STATUS:
        $payment->setState(self::DRUPAL_STATUS_COMPLETED);
        break;
      case Callback::CANCELLED_STATUS:
        $payment->setState(self::DRUPAL_STATUS_AUTHORIZATION_VOIDED);
        break;
    }
    $payment->save();
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getPaymentById($paymentId): PaymentInterface {
    // Load Drupal payment object.
    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $paymentStorage->loadByProperties([
      'remote_id' => $paymentId,
    ]);

    if (empty($payments)) {
      Drupal::logger('commerce_ecommpay')->error('No payments matching returned payment ID.');
      throw new InvalidResponseException('No payments matching returned payment ID.');
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = reset($payments);

    return $payment;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException|\Exception
   */
  public function onNotify(Request $request) {
    $config = $this->getConfiguration();

    // Check if secret_key config is present.
    if (empty($config['secret_key'])) {
      Drupal::logger('commerce_ecommpay')->error('The "Secret Key" configuration value must be set.');
      throw new PaymentGatewayException('The "Secret Key" configuration value must be set.');
    }

    try {
      $callback = $this->client->parseCallbackBody($config['secret_key'], $request->getContent());
    }
    catch (Exception $e) {
      Drupal::logger('commerce_ecommpay')->error($e->getMessage());
      throw new InvalidResponseException($e->getMessage());
    }

    // Get payment Id.
    $paymentId = $callback->getPaymentId();
    $payment = $this->getPaymentById($paymentId);

    // Handle statuses accordingly.
    switch ($callback->getPaymentStatus()) {
      // Success.
      case Callback::SUCCESS_STATUS:
        $payment->setState(self::DRUPAL_STATUS_COMPLETED);
        break;

      // Awaiting Capture.
      case Callback::AW_CAP_STATUS:
        $payment->setState(self::DRUPAL_STATUS_AUTHORIZATION);
        break;
    }

    $payment->setRemoteState($callback->getPaymentStatus());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $config = $this->getConfiguration();

    // Refund payment.
    try {
      $state = $this->client->refundPayment(
        (int) $config['project_id'],
        $config['secret_key'],
        $payment->getRemoteId(),
        $this->minorUnitsConverter->toMinorUnits($amount),
        $amount->getCurrencyCode()
      );

      if ($state['status'] !== self::STATUS_SUCCESS) {
        Drupal::logger('commerce_ecommpay')->error('Refund failed for payment ID @id. API response: @response', [
          '@id' => $payment->id(),
          '@response' => json_encode($state),
        ]);
        throw new PaymentGatewayException('Refund request failed. Please check the logs for details.');
      }

      $newState = $state['data']['payment_state'] ?? self::STATUS_PARTIALLY_REFUNDED;
      $payment->setState($newState);
      $payment->set('remote_state', $state['data']['remote_state'] ?? $newState);
      $payment->save();

    } catch (Exception $e) {
      Drupal::logger('commerce_ecommpay')->critical('Exception during refund for payment ID @id: @message', [
        '@id' => $payment->id(),
        '@message' => $e->getMessage(),
      ]);
      throw new PaymentGatewayException('An error occurred while processing the refund.');
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $config = $this->getConfiguration();

    // Capture payment.
    $state = $this->client->capturePayment(
      (int) $config['project_id'],
      $config['secret_key'],
      $payment->getRemoteId(),
      $this->minorUnitsConverter->toMinorUnits($amount),
      $amount->getCurrencyCode());

    if ($state['status'] != self::STATUS_SUCCESS) {
      Drupal::logger('commerce_ecommpay')->info('Payment was not captured.');
      throw new PaymentGatewayException('The was a problem capturing your payment. Please try again.');
    }
    else {
      $payment->setState(self::DRUPAL_STATUS_COMPLETED);
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function voidPayment(PaymentInterface $payment) {
    $config = $this->getConfiguration();

    // Capture payment.
    $state = $this->client->voidPayment(
      (int) $config['project_id'],
      $config['secret_key'],
      $payment->getRemoteId());

    if ($state['status'] != self::STATUS_SUCCESS) {
      Drupal::logger('commerce_ecommpay')->info('Payment was not voided.');
      throw new PaymentGatewayException('The was a problem voiding your payment. Please try again.');
    }
    else {
      $payment->setState('voided');
      $payment->save();
    }

  }

}
