<?php

namespace Drupal\commerce_ecommpay;

use Drupal;
use ecommpay\Callback;
use Exception;
use GuzzleHttp\ClientInterface;
use ecommpay\SignatureHandler;
use ecommpay\Payment;
use ecommpay\Gate;
use ecommpay\exception\ValidationException;
use Drupal\address\AddressInterface;

/**
 * Client for Ecommpay API.
 */
class EcommpayClient {

  public const API_HOST = 'https://api.ecommpay.com';
  private const API_VERSION = 'v2';
  private const API_PAYMENT_PATH = 'payment';
  private const API_REFUND_PATH = 'card/refund';
  private const API_CANCEL_PATH = 'card/cancel';
  private const API_CAPTURE_PATH = 'card/capture';
  private const API_STATUS_PATH = 'status';
  private const DESCRIPTION_REFUND = 'refund';
  private const DESCRIPTION_CAPTURE = 'capture';
  public const OPERATION_TYPE_SALE = 'sale';
  public const OPERATION_TYPE_AUTH = 'auth';
  private const REDIRECT_MODE_PARENT_PAGE = 'parent_page';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * Constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Returns payment page URL.
   *
   * @param string $projectId
   *   Project ID.
   * @param string $secretKey
   *   Secret key.
   * @param string $paymentId
   *   Payment ID.
   * @param string $customerId
   *   Customer ID.
   * @param string $email
   *   Customer email.
   * @param \Drupal\address\AddressInterface|null $billingAddress
   *   Billing address.
   * @param int $amount
   *   Amount in cents.
   * @param string $currencyCode
   *   Currency code.
   * @param string $successUrl
   *   Success URL.
   * @param string $failureUrl
   *   Failure URL.
   * @param string $notifyUrl
   *   Notify URL.
   * @param string $languageCode
   *   Language.
   * @param string $cardOperationType
   *   Card operation type.
   *
   * @return string
   *   Payment page URL.
   * @throws \ecommpay\exception\ValidationException
   */
  public function getPaymentPageUrl(
    string $projectId,
    string $secretKey,
    string $paymentId,
    string $customerId,
    string $email,
    ?AddressInterface $billingAddress,
    int $amount,
    string $currencyCode,
    string $successUrl,
    string $failureUrl,
    string $notifyUrl,
    string $languageCode,
    string $cardOperationType = self::OPERATION_TYPE_SALE): string {
    $ecommpayPayment = new Payment($projectId, $paymentId);
    $ecommpayPayment->setPaymentAmount($amount)
      ->setPaymentCurrency($currencyCode)
      ->setLanguageCode($languageCode)
      ->setCardOperationType($cardOperationType)

      // Sort out redirect urls and modes.
      ->setMerchantSuccessUrl($successUrl)
      ->setMerchantFailUrl($failureUrl)
      ->setRedirectSuccessUrl($successUrl)
      ->setRedirectFailUrl($failureUrl)
      ->setRedirectSuccessMode(self::REDIRECT_MODE_PARENT_PAGE)
      ->setRedirectFailMode(self::REDIRECT_MODE_PARENT_PAGE)
      ->setMerchantCallbackUrl($notifyUrl)

      // Customer details.
      ->setCustomerId($customerId)
      ->setCustomerEmail($email);

    if (!empty($billingAddress)) {
      $ecommpayPayment
        ->setCardHolder(implode(
          ' ',
          array_filter([$billingAddress->getGivenName(),
            $billingAddress->getFamilyName(),
          ])
        ))
        ->setCustomerFirstName($billingAddress->getGivenName())
        ->setCustomerLastName($billingAddress->getFamilyName())
        ->setCustomerAddress($billingAddress->getAddressLine1())
        ->setCustomerCity($billingAddress->getLocality())
        ->setCustomerCountry($billingAddress->getCountryCode())
        ->setCustomerPostal($billingAddress->getPostalCode())
        ->setCustomerRegion($billingAddress->getAdministrativeArea())

        ->setBillingFirstName($billingAddress->getGivenName())
        ->setBillingLastName($billingAddress->getFamilyName())
        ->setBillingAddress($billingAddress->getAddressLine1())
        ->setBillingCity($billingAddress->getLocality())
        ->setBillingCountry($billingAddress->getCountryCode())
        ->setBillingPostal($billingAddress->getPostalCode())
        ->setBillingRegion($billingAddress->getAdministrativeArea());
    }

    $gate = new Gate($secretKey);

    try {
      $url = $gate->getPurchasePaymentPageUrl($ecommpayPayment);
    }
    catch (ValidationException $e) {
      Drupal::logger('ecommpay')->error($e->getFormattedMessage());
      throw $e;
    }

    return $url;
  }

  /**
   * @throws \Exception
   */
  public function refundPayment(int $projectId, string $secretKey, string $paymentId, int $amount, string $currencyCode): array {
    return $this->sendPaymentRequest(self::API_REFUND_PATH, $projectId, $secretKey, $paymentId, [
      "payment" => [
        "description" => self::DESCRIPTION_REFUND,
        "amount" => $amount,
        "currency" => $currencyCode,
      ],
    ]);
  }

  /**
   * @param string $requestUri
   * @param int $projectId
   * @param string $secretKey
   * @param string $paymentId
   * @param array $additionalData
   *
   * @return array
   * @throws \Exception
   */
  private function sendPaymentRequest(string $requestUri, int $projectId, string $secretKey, string $paymentId,
    array $additionalData = []): array {

    // Form data.
    $url = join('/', [
      self::API_HOST,
      self::API_VERSION,
      self::API_PAYMENT_PATH,
      $requestUri
    ]);

    $data = [
      "general" => [
        "project_id" => $projectId,
        "payment_id" => $paymentId,
      ],
    ];
    $data = array_merge($data, $additionalData);
    $data['general']['signature'] = (new SignatureHandler($secretKey))->sign($data);

    // Post data to URL.
    $response = $this->client->post($url, ['json' => $data]);
    $response = json_decode($response->getBody()->getContents(), true);

    // Check if payment status is present.
    if (empty($response['status'])) {
      throw new Exception('Invalid response');
    }

    return $response;
  }

  /**
   * @throws \Exception
   */
  public function capturePayment(int $projectId, string $secretKey, string $paymentId, int $amount, string $currencyCode): array {
    return $this->sendPaymentRequest(self::API_CAPTURE_PATH, $projectId, $secretKey, $paymentId, [
      "payment" => [
        "description" => self::DESCRIPTION_CAPTURE,
        "amount" => $amount,
        "currency" => $currencyCode,
      ],
    ]);
  }

  /**
   * @throws \Exception
   */
  public function voidPayment(int $projectId, string $secretKey, string $paymentId): array {
    return $this->sendPaymentRequest(self::API_CANCEL_PATH, $projectId, $secretKey, $paymentId);
  }

  /**
   * @throws \Exception
   */
  public function getPaymentState(int $projectId, string $secretKey, string $paymentId): string {
    $response = $this->sendPaymentRequest(self::API_STATUS_PATH, $projectId, $secretKey, $paymentId);
    return $response['payment']['status'];
  }

  /**
   * Parses callback body.
   *
   * @param string $secretKey
   *   Secret key.
   * @param string $requestBody
   *   Request body.
   *
   * @return \ecommpay\Callback
   *   Callback object.
   * @throws \Exception
   */
  public function parseCallbackBody(string $secretKey, string $requestBody): Callback {
    // Create callback object with raw data.
    $gate = new Gate($secretKey);
    $callback = $gate->handleCallback($requestBody);

    // Check integrity by testing the signature.
    if (!$callback->checkSignature()) {
      Drupal::logger('commerce_ecommpay')->error('Invalid signature when processing callback');
      throw new Exception('Invalid signature.');
    }

    return $callback;
  }

}
