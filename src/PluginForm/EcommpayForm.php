<?php

namespace Drupal\commerce_ecommpay\PluginForm;

use Drupal;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use ecommpay\exception\ValidationException;
use Drupal\user\Entity\User;
use Drupal\commerce_ecommpay\EcommpayClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Uuid\Php;

/**
 * Class EcommpayForm.
 */
class EcommpayForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {
  /**
   * The ECOMMPAY client.
   *
   * @var \Drupal\commerce_ecommpay\EcommpayClient
   */
  protected $client;

  /**
   * Converter of minor units.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   * */
  protected $minorUnitsConverter;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   * */
  protected $languageManager;

  /**
   * UUID generator.
   *
   * @var \Drupal\Component\Uuid\Uuid
   * */
  protected $uuid;

  /**
   * Constructs a new EcommpayForm object.
   *
   * @param \Drupal\commerce_ecommpay\EcommpayClient $client
   *   The ECOMMPAY client.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minorUnitsConverter
   *   Converter of minor units.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Component\Uuid\Php $uuid
   *   The UUID generator.
   */
  public function __construct(EcommpayClient $client, MinorUnitsConverterInterface $minorUnitsConverter, LanguageManagerInterface $languageManager, Php $uuid) {
    $this->client = $client;
    $this->minorUnitsConverter = $minorUnitsConverter;
    $this->languageManager = $languageManager;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EcommpayForm {
    // Instantiates this form class.
    return new static(
      $container->get('ecommpay.ecommpay_client'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('language_manager'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\commerce\Response\NeedsRedirectException|\Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $paymentGatewayPlugin */
    $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $config = $paymentGatewayPlugin->getConfiguration();

    if (empty($config['project_id'])) {
      throw new PaymentGatewayException('The "Project ID" configuration value must be set.');
    }

    if (empty($config['secret_key'])) {
      throw new PaymentGatewayException('The "Secret Key" configuration value must be set.');
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // We generate our own UUID here to make sure this value is always unique.
    // ECOMMPAY requires this value to be unique per project.
    // We also allow other modules to alter this value if need be.
    $paymentId = $this->uuid->generate();
    Drupal::moduleHandler()->alter('commerce_ecommpay_payment_id', $paymentId, $payment);

    $url = $this->getUrl($paymentId, $order, $payment, $form);
    $this->savePaymentId($payment, $paymentId);

    // Manage presentation, redirect, modal or iframe.
    $form['container'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $url,
        'width' => "100%",
        'height' => "600px",
        'loading' => 'eager',
        'class' => ['ecommpay-payment-iframe'],
      ],
    ];

    // Attach url to drupal settings.
    $form['#attached']['drupalSettings']['ecommpay'] = [
      'url' => $url,
    ];

    return $this->buildRedirectForm($form, $form_state, $url, []);
  }

  /**
   * @param string $paymentId
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param array $form
   *
   * @return string
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getUrl(
    string $paymentId,
    Drupal\commerce_order\Entity\OrderInterface $order,
    Drupal\commerce_payment\Entity\PaymentInterface $payment,
    array $form
  ): string {

    /** @var \Drupal\user\Entity\User|null $user */
    $user = User::load(Drupal::currentUser()->id());
    $customerId = $user ? $user->uuid() : $this->uuid->generate();

    // Get billing address.
    /** @var \Drupal\address\AddressInterface $billingAddress */
    $billingAddress = NULL;
    if ($order->getBillingProfile()) {
      $billingAddress = $order->getBillingProfile()->get('address')->first();
    }

    $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $config = $paymentGatewayPlugin->getConfiguration();

    try {
      return $this->client->getPaymentPageUrl(
        $config['project_id'],
        $config['secret_key'],
        $paymentId,
        $customerId,
        $order->getEmail(),
        $billingAddress,
        $this->minorUnitsConverter->toMinorUnits($order->getTotalPrice()),
        $order->getTotalPrice()->getCurrencyCode(),
        $form['#return_url'],
        $form['#return_url'],
        $paymentGatewayPlugin->getNotifyUrl()->toString(),
        $this->languageManager->getCurrentLanguage()->getId(),
        $form['#capture'] ? EcommpayClient::OPERATION_TYPE_SALE : EcommpayClient::OPERATION_TYPE_AUTH
      );
    }
    catch (ValidationException $e) { // Validating possible exceptions
      Drupal::logger('ecommpay')->error($e->getFormattedMessage());
      throw new PaymentGatewayException($e->getFormattedMessage());
    }
  }

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param string $paymentId
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function savePaymentId(Drupal\commerce_payment\Entity\PaymentInterface $payment, string $paymentId) {
    // Set remote_payment_id for payment and save payment object.
    $payment->setRemoteId($paymentId);
    $payment->save();
  }
}
